import Foo from './components/Foo/Foo.vue';
import Bar from './components/Bar/Bar.vue';
import List from './components/List/List.vue';

export default [
    { path: '/', component: List },
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar }
]