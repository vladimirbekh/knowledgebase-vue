import Vue from 'vue';
import VueRouter from 'vue-router';
import regeneratorRuntime from 'regenerator-runtime'; // for support async functions
import Root from './components/Root/Root.vue';

Vue.use(VueRouter);

import routes from './routers';
const router = new VueRouter({ mode: 'history', routes });

new Vue({
    router,
    render: function (createElement) {
      return createElement(Root)
    },
    el: '#vue-root'
})